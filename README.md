# DevOps Task 3

## Golang "REST" server
### Description
Simple Golang application, which simulate a dummy REST server. It does not have any routing, instead it return a path of the request:
- `curl http://localhost:8080/foo` will return "Hello, /foo"
- `curl http://localhost:8080/foo/bar` will return "Hello, /foo/bar"

### Run example
To run application open a console and type:
```bash
$ cd $project_path/app
$ go run main.go
```
Then the application will start and handle all request sent to `localhost:8080`.
To stop the application press `ctrl+c`.

## Docker container
### Purpose
Running Go application in container will improve developement and also deployment. It allows also using modern tools like
Kubernetes, Swarm, etc.

Go application Docker images can be build in two ways:
- inherit from Go docker image, add own application code and use `go install <src>`
- compile and build static linked binary, create Docker image from scratch and add only this particular binary
 
The second option will create much smaller image (270MB vs 7MB). Container should be also more performant.

### Build
```bash
$ docker build --pull -t registry.gitlab.com/dzinek/devops-task-3/go:latest -f docker/go/Dockerfile .
$ docker build --pull -t registry.gitlab.com/dzinek/devops-task-3/scratch:latest -f docker/scratch/Dockerfile .
```


### Run
```bash
$ docker run --publish 8080:8080 --name devops-task-3_go --rm registry.gitlab.com/dzinek/devops-task-3/go:latest
$ docker run --publish 8080:8080 --name devops-task-3_scratch --rm registry.gitlab.com/dzinek/devops-task-3/scratch:latest
```

## Terraform Docker Swarm description

Swarm should be created only in 3AZ locations.
There should be at least 3 Swarm managers - one per each AZ.
There should be at least 3 Swarm nodes - two per each AZ.
Swarm nodes should be configured in Auto Scaling Groups - in this example there are fixed numbers of EC2s.

## Docker Swarm initialication
To create Swarm cluster
```bash
env ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -b -i swarm-inventory swarm-init.yml
```
## Docker Swarm deploy service
To deploy `devops-task-3` service on Swarm cluster:
```bash
env ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -b -i swarm-inventory swarm-deploy.yml
```