package rest_server

import (
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
)

func StartServer() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %q !. \n", html.EscapeString(r.URL.Path))
		fmt.Fprintf(w, "Executed on %q \n", r.Host)

		hostname, err := os.Hostname()
		if err != nil {
			panic(err)
		}

		fmt.Fprintf(w, "Hostname/Containter ID: %q \n", hostname)
	})

	log.Println(http.ListenAndServe(":8080", nil))

}