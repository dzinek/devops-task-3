##General vars
variable "ssh_user" {
  default = "ubuntu"
}
variable "public_key_path" {
  default = "/tmp/.ssh/id_rsa.pub"
}
##GCE Specific Vars
variable "gce_worker_count" {
  default = 1
}
variable "gce_creds_path" {
  default = "/tmp/gae_key.json"
}
variable "gce_project" {
  default = "devops-task-3"
}
variable "gce_region" {
  default = "europe-west1"
}
variable "gce_instance_size" {
  default = "n1-standard-1"
}