##General vars
variable "ssh_user" {
  default = "ubuntu"
}
variable "public_key_path" {
  default = "/tmp/.ssh/id_rsa.pub"
}
##AWS Specific Vars
variable "aws_access_key" {
  default = "access-key"
}
variable "aws_secret_key" {
  default = "secret-key"
}
variable "aws_worker_count" {
  default = 1
}
variable "aws_key_name" {
  default = "dzinek-key"
}
variable "aws_instance_size" {
  default = "t2.micro"
}
variable "aws_region" {
  default = "eu-central-1"
}
##GCE Specific Vars
variable "gce_worker_count" {
  default = 1
}
variable "gce_creds_path" {
  default = "/tmp/gae_key.json"
}
variable "gce_project" {
  default = "devops-task-3"
}
variable "gce_region" {
  default = "europe-west"
}
variable "gce_instance_size" {
  default = "n1-standard-1"
}