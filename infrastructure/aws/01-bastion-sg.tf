resource "aws_security_group" "sg_swarm_bastion" {
  name        = "${var.aws_vpc_key}-sg-swarm-bastion"
  description = "Security group for swarm cluster bastion instances"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  tags {
    Name = "${var.aws_vpc_key}-sg-swarm-bastion"
    VPC = "${var.aws_vpc_key}"
    Terraform = "Terraform"
  }
}

output "sg_bastion" {
  value = "${aws_security_group.sg_swarm_bastion.id}"
}