resource "aws_instance" "swarm-bastion" {
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  count                       = "1"
  associate_public_ip_address = "true"
  key_name                    = "${var.aws_key_name}"
  subnet_id                   = "${aws_subnet.a.id}"
  vpc_security_group_ids      = [
    "${aws_security_group.sg_swarm_bastion.id}"
  ]

  tags {
    Name = "${var.aws_vpc_key}-swarm-bastion"
    VPC = "${var.aws_vpc_key}"
    Terraform = "Terraform"
  }
}

output "bastion_host_dns" {
  value = "${aws_instance.swarm-bastion.public_dns}"
}


output "bastion_host_ip" {
  value = "${aws_instance.swarm-bastion.public_ip}"
}