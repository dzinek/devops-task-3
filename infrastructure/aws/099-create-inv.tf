resource "null_resource" "ansible-provision" {
  depends_on = ["aws_instance.swarm-bastion", "aws_instance.swarm-manager", "aws_instance.swarm-node"]

  provisioner "local-exec" {
    command = "echo \"[swarm-bastion]\" > swarm-inventory"
  }

  provisioner "local-exec" {
    command = "echo \"${format("%s ansible_ssh_user=%s", aws_instance.swarm-bastion.0.public_ip, var.ssh_user)}\" >> swarm-inventory"
  }

  provisioner "local-exec" {
    command = "echo \"[swarm-manager]\" >> swarm-inventory"
  }

  provisioner "local-exec" {
    command = "echo \"${join("\n",formatlist("%s ansible_ssh_user=%s", aws_instance.swarm-manager.*.private_ip, var.ssh_user))}\" >> swarm-inventory"
  }

  provisioner "local-exec" {
    command = "echo \"[swarm-nodes]\" >> swarm-inventory"
  }

  provisioner "local-exec" {
    command = "echo \"${join("\n",formatlist("%s ansible_ssh_user=%s", aws_instance.swarm-node.*.private_ip, var.ssh_user))}\" >> swarm-inventory"
  }


  provisioner "local-exec" {
    command = "echo \"Host ${join(" ", aws_instance.swarm-node.*.private_ip)} ${join(" ", aws_instance.swarm-manager.*.private_ip)}\" > ssh.cfg"
  }

  provisioner "local-exec" {
    command = "echo \"ProxyCommand    ssh -W %h:%p ${format("%s@%s", var.ssh_user, aws_instance.swarm-bastion.0.public_ip)}\" >> ssh.cfg"
  }

  provisioner "local-exec" {
    command = "echo \"\" >> ssh.cfg"
  }

  provisioner "local-exec" {
    command = "echo \"Host *\" >> ssh.cfg"
  }

  provisioner "local-exec" {
    command = "echo \"ControlMaster   auto\" >> ssh.cfg"
  }

  provisioner "local-exec" {
    command = "echo \"ControlPath     ~/.ssh/ansible-%r@%h:%p\" >> ssh.cfg"
  }

  provisioner "local-exec" {
    command = "echo \"ControlPersist  15m\" >> ssh.cfg"
  }

  provisioner "local-exec" {
    command = "echo \"[ssh_connection]\" > ansible.cfg"
  }

  provisioner "local-exec" {
    command = "echo \"ssh_args = -F ssh.cfg\" >> ansible.cfg"
  }

  provisioner "local-exec" {
    command = "echo \"control_path = ~/.ssh/ansible-%r@%h:%p\" >> ansible.cfg"
  }
}
