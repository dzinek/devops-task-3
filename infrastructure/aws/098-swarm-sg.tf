resource "aws_security_group" "swarm_sg" {
  name        = "${var.aws_vpc_key}-sg-swarm"
  description = "Security group for swarm cluster instances"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    from_port   = 2375
    to_port     = 2377
    protocol    = "tcp"
    cidr_blocks = [
      "${aws_vpc.vpc.cidr_block}"
    ]
    security_groups = [
      "${aws_security_group.sg_swarm_elb.id}",
    ]
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "tcp"
    cidr_blocks = [
      "${aws_vpc.vpc.cidr_block}"
    ]
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "udp"
    cidr_blocks = [
      "${aws_vpc.vpc.cidr_block}"
    ]
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "tcp"
    cidr_blocks = [
      "${aws_vpc.vpc.cidr_block}"
    ]
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    cidr_blocks = [
      "${aws_vpc.vpc.cidr_block}"
    ]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.sg_swarm_elb.id}",
    ]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${aws_vpc.vpc.cidr_block}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  tags {
    Name = "${var.aws_vpc_key}-sg-swarm"
    VPC = "${var.aws_vpc_key}"
    Terraform = "Terraform"
  }
}

output "sg_swarm" {
  value = "${aws_security_group.swarm_sg.id}"
}