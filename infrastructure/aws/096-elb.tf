resource "aws_elb" "swarm_elb" {
  name            = "${var.aws_vpc_key}-swarm-elb"
  internal        = false
  subnets         = ["${aws_subnet.a.id}"]
  security_groups = ["${aws_security_group.sg_swarm_elb.id}"]
  instances       = ["${aws_instance.swarm-manager.0.id}", "${aws_instance.swarm-node.*.id}"]

  health_check {
    healthy_threshold = 2
    interval = 10
    target = "HTTP:8080/"
    timeout = 2
    unhealthy_threshold = 4
  }

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  tags {
    Name = "${var.aws_vpc_key}-swarm-elb"
    VPC = "${var.aws_vpc_key}"
    Terraform = "Terraform"
  }
}

output "swarm_elb" {
  value = "${aws_elb.swarm_elb.dns_name}"
}