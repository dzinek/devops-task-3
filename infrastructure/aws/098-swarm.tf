##Create Swarm Master Instance
resource "aws_instance" "swarm-manager" {
  depends_on             = ["aws_security_group.swarm_sg"]
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "${var.aws_instance_size}"
  vpc_security_group_ids = ["${aws_security_group.swarm_sg.id}"]
  key_name               = "${var.aws_key_name}"
  count                  = "${var.aws_swarm_managers_count}"
  subnet_id              = "${aws_subnet.a.id}"
  associate_public_ip_address = true

  tags {
    Name = "${var.aws_vpc_key}-manager-${count.index}"
    VPC = "${var.aws_vpc_key}"
    Terraform = "Terraform"
  }

}

##Create AWS Swarm Workers
resource "aws_instance" "swarm-node" {
  depends_on             = ["aws_security_group.swarm_sg", "aws_instance.swarm-manager"]
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "${var.aws_instance_size}"
  vpc_security_group_ids = ["${aws_security_group.swarm_sg.id}"]
  key_name               = "${var.aws_key_name}"
  count                  = "${var.aws_swarm_nodes_count}"
  subnet_id              = "${aws_subnet.a.id}"
  associate_public_ip_address = true

  tags {
    Name = "${var.aws_vpc_key}-node-${count.index}"
    VPC = "${var.aws_vpc_key}"
    Terraform = "Terraform"
  }
}

output "swarm_managers" {
  value = "${aws_instance.swarm-manager.*.private_ip}"
}

output "swarm_nodes" {
  value = "${aws_instance.swarm-node.*.private_ip}"
}