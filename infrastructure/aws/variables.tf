##General vars
variable "ssh_user" {
  default = "ubuntu"
}
variable "public_key_path" {
  default = "/tmp/.ssh/id_rsa.pub"
}
##AWS Specific Vars
variable "aws_access_key" {
  default = "access-key"
}
variable "aws_secret_key" {
  default = "secret-key"
}
variable "aws_swarm_managers_count" {
  default = 1
}
variable "aws_swarm_nodes_count" {
  default = 2
}
variable "aws_key_name" {
  default = "dzinek-key"
}
variable "aws_instance_size" {
  default = "t2.micro"
}
variable "aws_region" {
  default = "eu-central-1"
}
variable "aws_vpc_cidr" {
  default = "172.31.0.0/16"
}
variable "aws_vpc_key" {
  default = "devops-task-3"
}
