package main

import (
	"gitlab.com/dzinek/devops-task-3/rest_server"
)

func main() {
	rest_server.StartServer()
}
